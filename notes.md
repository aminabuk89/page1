                       2.1. Додати нове домашнє завдання в новий репозиторій на Gitlab.
                1. Створюємо новий репозиторій:
- Заходимо на сайт git.lab.com.
- Натискаємо на + (зліва вгорі).
- Далі "New project/repository".
- Натискаємо "Create blank project", наприклад, Page1.
- У "Project name" вводимо назву проекту.
- У "Project deployment target (optional)" вибираємо "Select the deployment target".
- У "Visibility Level" змінюємо вибір на "Public".
- У "Project Configuration" прибираємо ✔️.
- Натискаємо "Create project".

               2. Створюємо нове домашнє завдання:

- На робочому столі створюємо папку, наприклад, Page1 і в цю папку вкладаємо файл, наприклад, notes.md
- Входимо в нашу папку.
- В Page1 відкриваємо git bash (натискаємо в папці правой кнопкой миші і вибираємо "показати додаткові параметри", далі зі списку вибираємо "open git bash here").
- У відкритому вікні пишемо "git clone", та вставляємо url, скопійований з "Create a new repository", натискаємо enter (після цього у page1 повинна зявитися папка з назвою .git)
- Далі пишемо:
   1. git add . 
   2. git commit -m "adding file and folder"
   3. git push 
   Після кожної команти натискаємо enter.
- Після цього переходимо на сайт git.lab.com, в розділі "Project" вибираємо наш проект - Page1.
- Оновлює сторінку.
- Бачимо, як в Page1 з’явився файл з назвою notes.md


                        2.2. Додати існуюче домашнє завдання в новий репозиторій на Gitlab.
                1. Створюємо новий репозиторій:
- Заходимо на сайт git.lab.com.
- Натискаємо на + (зліва вгорі).
- Далі "New project/repository".
- Натискаємо "Create blank project", наприклад, Page2.
- У "Project name" вводимо назву проекту.
- У "Project deployment target (optional)" вибираємо "Select the deployment target".
- У "Visibility Level" змінюємо вибір на "Public".
- У "Project Configuration" прибираємо ✔️.
- Натискаємо "Create project".

                2. Додаємо існуюче домашнє завдання:
                
- Входимо в папку, де в нас вже є створені файли.
- Відкриваємо git bash (натискаємо в папці правой кнопкой миші і вибираємо "показати додаткові параметри", далі зі списку вибираємо "open git bash here").
- Після цього вводимо команду git init
- Далі пишемо git remote add origin і вставляємо url, скопійований з "Create a new repository", натискаємо enter (після цього у page2 повинна зявитися папка з назвою .git)
- Далі пишемо:
   1. git add . 
   2. git commit -m "adding file and folder"
   3. git push 
   Після кожної команти натискаємо enter.
- Після цього переходимо на сайт git.lab.com, в розділі "Project" вибираємо наш проект - Page2.
- Оновлює сторінку.
- Бачимо, як в Page2 з’явився файл з нашим домашнім завданням.

                
